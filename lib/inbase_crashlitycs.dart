library inbase_crashlitycs;

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:inbase/InBase.dart';
import 'package:inbase/InBaseConfig.dart';
import 'package:inbase/Utilities.dart';
import 'model/CrashReportModel.dart';
import 'package:flutter/foundation.dart';

class InBaseCrashlitycs {
  static const String apiEndpoint =
      'https://inbase.webinify.com/api/v1/crashlitycs';

  /// This function will keep a track of all unhandled errors crashes of the app
  static Future<void> recordFlutterFatalError(FlutterErrorDetails details) async {
    await InBaseCrashlitycs.report(CrashReportModel(
        exception: details.exception,
        context: details.context,
        library: details.library,
        stackTrace: details.stack));
  }

  /// This function will create a report on the website
  static Future<void> report(CrashReportModel crashReportModel) async {
    /// We need to get the global waterbase config
    InBaseConfig inBaseConfig = await InBaseConfig.getConfig();

    Map<String, String> headers = {
      "X-PROJECT-ID": inBaseConfig.projectId.toString(),
      "X-API-KEY": inBaseConfig.apiKey
    };

    try {
      http.Response response = await http.post(Uri.parse(apiEndpoint),
          headers: headers, body: crashReportModel.toJson());

      if (response.statusCode == 200) {
        /// The request did work

      }
    } catch (e) {
      /// We will always log errors
      Utilities.appLog("Couldn't send crash report");

      /// We'll show errors only if allowed in inBaseConfig settings
      if(inBaseConfig.showToastOnError == true) {
        Utilities.showToast("Couldn't send crash report");
      }
    }
  }
}
