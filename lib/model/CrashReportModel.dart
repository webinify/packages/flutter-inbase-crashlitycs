import 'package:flutter/material.dart';
import 'package:inbase_crashlitycs/CrashlitycsType.dart';

class CrashReportModel {
  CrashReportModel(
      {this.context,
      this.exception,
      this.stackTrace,
      this.library,
      this.createdAt,
      this.userId,
      this.description,
      this.url,
      this.type,
      this.reason});

  /// Auto generated DateTime of the report
  DateTime? createdAt = DateTime.now();

  /// You can link the report to a specific user id
  int? userId;

  /// Use this in case you need to know the context details
  DiagnosticsNode? context;

  /// The come from FlutterError
  Object? exception;

  /// Which library is faulty
  String? library;

  /// Get the stack trace
  StackTrace? stackTrace;

  /// Here we define by default http request
  /// But you can override this
  /// This value is used to know in which type of context the error happens
  ///
  /// CrashlitycsType.http is when you tried a http request that failed for some reason
  ///
  CrashlitycsType? type = CrashlitycsType.http;

  /// This is the reason of the crash
  /// You can write here if you expect a reason of crash depending on the function
  ///
  /// For example:
  /// You created a function with try catch and that function might take too long
  /// In this case you would expect a failure and report if it take more 3 seconds
  /// You write "Connection timed out"
  String? reason;

  /// The description might be useful to know what is happening before this request
  /// Don't sent any catch(e) here
  /// This description must be written by hands that explain the function previously launched
  String? description;

  /// The url is useful when you want to keep a trace of the URL called for an API request for example
  String? url;

  Map<String, dynamic> toJson() => {
        "createdAt": createdAt.toString(),
        "userId": userId,
        "description": description,
        "reason": reason,
        "url": url,
        "type": type,
        "exception": exception.toString(),
        "context": context.toString(),
        "stackTrace": stackTrace.toString(),
        "library": library,
      };
}
