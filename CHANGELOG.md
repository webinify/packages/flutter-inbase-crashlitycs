## 0.0.7

* The toast notifications are now based on InBaseConfig
* Will always keep in logs errors
* InBase updated to 0.0.7

## 0.0.6

* CrashReportModel has been updated, some parameters removed like "message"
* added a new function to handle unhandled errors by using FlutterError.onError

## 0.0.5

* Fixes
* Added more fields

## 0.0.3

* Project initiated.
