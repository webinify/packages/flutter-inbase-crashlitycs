<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

InBaseCrashlitycs is a package meant to track any errors and crash of your flutter apps. 


## Features

-   [Work in progress] You can "try catch" errors and send them to your inbase.webinify.com account
-   [Coming soon] You can also send logs when the app crash by sending a crashReport

## Getting started

To use this package, dont forget to also load our inbase package


## Usage

```dart
Future<void> asyncFunction() async {
    try {
        "your code here";
    } catch(e) {
        await InBaseCrashlitycs.report(CrashReportModel(message: e.toString()));
    }
}
```

## Additional information

This package require inbase to be installed in order to initiate the authentication to inbase.webinify.com

Note that this package is still under development, at this state it is not usable.
